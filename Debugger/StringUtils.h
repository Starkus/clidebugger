#pragma once

namespace Str
{
	const size_t DEFAULT_SIZE = 10;

	void FromInt(long n, char *buffer, size_t size = DEFAULT_SIZE);
	void FromFloat(double n, char *buffer, size_t size = DEFAULT_SIZE);

	int Cmp(const char *a, const char *b);
	int CmpNoCase(const char *a, const char *b);
	bool ArePathsEqual(const char *a, const char *b);
	void CopyStr(const char *source, char *dest);
	void CopyStr(const char *begin, const char *end, char *dest);
	void CopyStr(const wchar_t *source, char *dest);
	void FilePathTruncateLast(const char *source, char *dest);
}
