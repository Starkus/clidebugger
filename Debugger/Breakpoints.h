#pragma once

#include <Windows.h>
#include <DbgHelp.h>
#include <unordered_map>

class Symbols;

class Breakpoints
{
public:
	void Initialize(HANDLE processHandle, DWORD64 baseOfImage, Symbols *symbols);
	DWORD64 RegisterBreakpoint(DWORD64 address, bool persist);
	DWORD64 RegisterBreakpoint(const char *file, const int line, bool persist);
	bool UnregisterBreakpoint(DWORD64 address);
	bool NotifyBreakpoint(ULONG64 programCounter, HANDLE thread, DEBUG_EVENT &debugEvent);

	DWORD64 Find(const char *file, const int line);
	bool IsActive(DWORD64 address);

private:
	struct Breakpoint
	{
		DWORD64 mAddress = NULL;
		bool mPersist;
		BYTE mReplacedMemory;
	};

	DWORD64 GetAddressFromFileLine(const char *file, const int line);

	Symbols *mSymbols;
	HANDLE mProcessHandle;
	DWORD64 mBaseOfImage;
	std::unordered_map<ULONG64, Breakpoint> mBreakpointsByAddress;
};
