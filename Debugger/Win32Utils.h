#pragma once

#include <Windows.h>
#include <DbgHelp.h>
#include <stdio.h>
#include <assert.h>

#pragma unmanaged

#ifdef _DEBUG
#define DEBUG
#endif

void Win32PrintError(const char *funcName, const char *file, const int line, ...);
DWORD Win32WriteConsole(const char *message);

#ifdef DEBUG
#define LOGF(FORMAT, ...) do { \
	char BREAKMSGBUFFER[256]; \
	sprintf_s(BREAKMSGBUFFER, 256, FORMAT, __VA_ARGS__); \
	OutputDebugStringA(BREAKMSGBUFFER); \
} while (false)

#define BREAK __debugbreak
#define ASSERT(expr) do { if (!(expr)) { BREAK(); } } while(false)

#define BREAK_MSG(FORMAT, ...) do { \
	LOGF(FORMAT, __VA_ARGS__); \
	BREAK(); \
} while (false)

#define PRINTERROR(funcName, ...) Win32PrintError(funcName, __FILE__, __LINE__, __VA_ARGS__)

#define WIN32_WARN(FUNCCALL) do {if (!FUNCCALL) PRINTERROR(#FUNCCALL);} while(false)
#define WIN32_ASSERT(FUNCCALL) do {if (!FUNCCALL) { PRINTERROR(#FUNCCALL); ASSERT(false); } } while(false)
#define WIN32_RETURN(FUNCCALL) do {if (!FUNCCALL) return;} while(false)
#define WIN32_RETURNV(FUNCCALL, RETURNVALUE) do {if (!FUNCCALL) return RETURNVALUE;} while(false)
#define WIN32_WARNRETURN(FUNCCALL) do {if (!FUNCCALL) { PRINTERROR(#FUNCCALL); return; } } while(false)
#define WIN32_WARNRETURNV(FUNCCALL, RETURNVALUE) do {if (!FUNCCALL) { PRINTERROR(#FUNCCALL); return RETURNVALUE; } } while(false)
#endif

#ifndef DEBUG
#define LOGF(FORMAT, ...)
#define BREAK()
#define ASSERT(expr)
#define BREAK_MSG(FORMAT, ...)
#define PRINTERROR(funcName, ...)
#define WIN32_WARN(FUNCCALL) do { FUNCCALL; } while(false)
#define WIN32_ASSERT(FUNCCALL) do { FUNCCALL; } while(false)
#define WIN32_RETURN(FUNCCALL) do { if (!FUNCCALL) return; } while(false)
#define WIN32_RETURNV(FUNCCALL, RETURNVALUE) do { if (!FUNCCALL) return RETURNVALUE; } while(false)
#define WIN32_WARNRETURN(FUNCCALL) do { if (!FUNCCALL) { return; } } while(false)
#define WIN32_WARNRETURNV(FUNCCALL, RETURNVALUE) do { if (!FUNCCALL) { return RETURNVALUE; } } while(false)
#endif
