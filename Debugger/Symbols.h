#pragma once

#include <Windows.h>
#include <DbgHelp.h>
#include <unordered_map>
#include <string>

enum class BasicType
{
	NotBasic	= 0,
	Void		= 1,
	Char		= 2,
	WChar		= 3,
	Int			= 6,
	UInt		= 7,
	Float		= 8,
	Bool		= 10,
	Long		= 13,
	ULong		= 14,
	UDT			= 15,
	Pointer		= 16
};

struct UDTInfo;
struct PointerInfo;

struct Symbol
{
	~Symbol();

	const char *name = nullptr;
	BasicType basicType;
	unsigned int size;
	union
	{
		uint64_t bytes;
		char asChar;
		wchar_t asWChar;
		int asInt;
		unsigned int asUInt;
		float asFloat;
		bool asBool;
		long asLong;
		unsigned long asULong;
		UDTInfo *asUDTInfo;
		PointerInfo *asPointerInfo;
	} data;
};

struct UDTInfo
{
	~UDTInfo();

	const char *name = nullptr;
	Symbol *children;
	DWORD childrenCount;
};

struct PointerInfo
{
	~PointerInfo();

	const char *name = nullptr;
	uint64_t address;
	Symbol *symbol;
};

struct SymbolEnumContext;
struct CallstackEntry;

class Symbols
{
public:
	~Symbols();

	friend BOOL CALLBACK sEnumModules(PCSTR moduleName, DWORD64 baseOfDll, void *symbolsPtr);
	friend BOOL CALLBACK sEnumModules(PCSTR moduleName, DWORD64 baseOfDll, void *symbolsPtr);

	typedef std::unordered_map<DWORD, SRCCODEINFO> LineMap;
	typedef std::unordered_map<std::string, LineMap> FileMap;

	bool IsInitialized();
	void Initialize(HANDLE processHandle, const char *searchPath);
	ULONG64 LoadModule(const char *moduleName, HANDLE fileHandle = NULL);
	void AddSymbol(std::string file, DWORD line, SRCCODEINFO symbol);
	bool GetSymbolInFileLine(std::string file, DWORD line, SRCCODEINFO &symbol);
	bool GetSymbolFromAddress(ULONG64 address, SYMBOL_INFO *symbol);
	bool GetLineFromAddress(ULONG64 address, IMAGEHLP_LINE64 &line);
	size_t GetLocalSymbols(CallstackEntry &callstackEntry, Symbol *buffer, size_t bufferSize);
	void ReadLocalSymbol(Symbol &symbol, const char *name, ULONG64 address, ULONG typeIndex, SymbolEnumContext &context);

private:
	void BuildSymbolsMap(ULONG64 baseAddress);

	void SetModuleBaseAddress(std::string moduleName, DWORD64 moduleBaseAddress);
	DWORD64 GetModuleBaseAddress(std::string moduleName);
	void GetSymbolName(ULONG typeIndex, char *const nameBuffer, SymbolEnumContext &context);
	void FillBasicTypeInfo(Symbol &symbol, ULONG typeIndex, SymbolEnumContext &context);
	void FillValue(Symbol &symbol, ULONG64 address, SymbolEnumContext &context);

	bool mIsInitialized = false;
	FileMap mFileMap;
	std::unordered_map<std::string, DWORD64> mModulesBaseAddresses;
	HANDLE mProcessHandle;
};
