#include "Symbols.h"

#include <algorithm>
#include <cctype>

#include "Debugger.h"
#include "StringUtils.h"
#include "Win32Utils.h"

static const size_t MAX_SYMBOL_NAME_SIZE = 64;

struct SymbolEnumContext
{
	Symbols *symbols;
	BYTE *stackMemory;
	size_t bytesRead;
	void *stackPointer;
	Symbol *returnBuffer;
	size_t bufferCapacity;
	size_t index;
	ULONG64 modBase;
};

static BOOL CALLBACK sEnumModules(PCSTR moduleName, DWORD64 baseOfDll, void *symbolsPtr)
{
	Symbols &symbols = *(Symbols *)symbolsPtr;
	symbols.SetModuleBaseAddress(moduleName, baseOfDll);
	symbols.BuildSymbolsMap(baseOfDll);
	return true;
}

static BOOL CALLBACK sEnumLinesCallback(SRCCODEINFO *lineInfo, void *symbolsPtr)
{
	Symbols &symbols = *(Symbols *)symbolsPtr;
	symbols.AddSymbol(lineInfo->FileName, lineInfo->LineNumber, *lineInfo);

	return true;
}

BOOL CALLBACK sEnumLocalSymbolsCallback(SYMBOL_INFO *symbolInfo, ULONG symbolSize, void *contextPtr)
{
	SymbolEnumContext &context = *(SymbolEnumContext *)contextPtr;

	if (symbolInfo)
	{
		context.modBase = symbolInfo->ModBase;
		Symbol &symbol = context.returnBuffer[context.index];

		ULONG64 address = symbolInfo->Address;
		if (symbolInfo->Flags & SYMFLAG_LOCAL)
			address += (ULONG64)(BYTE *)context.stackPointer;
		context.symbols->ReadLocalSymbol(symbol, symbolInfo->Name, address, symbolInfo->TypeIndex, context);

		++context.index;
		ASSERT(context.index < context.bufferCapacity);
		if (context.index >= context.bufferCapacity)
			return false;
	}
	return true;
}

Symbol::~Symbol()
{
	if (name)
		VirtualFree((void *)name, 0, MEM_RELEASE);
	if (basicType == BasicType::UDT)
	{
		delete (UDTInfo *)data.asUDTInfo;
	}
}

UDTInfo::~UDTInfo()
{
	if (name)
		VirtualFree((void *)name, 0, MEM_RELEASE);
	delete[] children;
}

PointerInfo::~PointerInfo()
{
	if (name)
		VirtualFree((void *)name, 0, MEM_RELEASE);
	if (symbol)
		delete symbol;
}

Symbols::~Symbols()
{
	CloseHandle(mProcessHandle);
}

bool Symbols::IsInitialized()
{
	return mIsInitialized;
}

void Symbols::Initialize(HANDLE processHandle, const char *searchPath)
{
	mProcessHandle = processHandle;

	SymSetOptions(SYMOPT_UNDNAME | SYMOPT_LOAD_LINES);

	WIN32_ASSERT(SymInitialize(mProcessHandle, searchPath, true));
	WIN32_ASSERT(SymEnumerateModules64(mProcessHandle, sEnumModules, this));

	mIsInitialized = true;
}

ULONG64 Symbols::LoadModule(const char *moduleName, HANDLE fileHandle)
{
	ULONG64 baseAddress = SymLoadModuleEx(mProcessHandle, fileHandle, moduleName, NULL, 0, 0, NULL, 0);
	if (baseAddress)
	{
		SetModuleBaseAddress(moduleName, baseAddress);
		BuildSymbolsMap(baseAddress);
		LOGF("Successfully loaded module %s\n", moduleName);
	}
	return baseAddress;
}

void Symbols::BuildSymbolsMap(ULONG64 baseAddress)
{
	const char *filenameWildCard = NULL;
	SymEnumLines(mProcessHandle, baseAddress, NULL, filenameWildCard, sEnumLinesCallback, this);
}

void Symbols::AddSymbol(std::string file, DWORD line, SRCCODEINFO symbol)
{
	std::transform(file.begin(), file.end(), file.begin(), [](unsigned char c)
		{
			return std::tolower(c);
		});
	auto fileFindRes = mFileMap.find(file);
	if (fileFindRes != mFileMap.end())
	{
		auto lineFindRes = fileFindRes->second.find(line);
		if (lineFindRes != fileFindRes->second.end())
		{
			// Add only if address is lower
			if (symbol.Address < lineFindRes->second.Address)
				lineFindRes->second = symbol;
			return;
		}
	}
	mFileMap[file][line] = symbol;
}

bool Symbols::GetSymbolInFileLine(std::string file, DWORD line, SRCCODEINFO &symbol)
{
	std::transform(file.begin(), file.end(), file.begin(), [](unsigned char c)
		{
			return std::tolower(c);
		});
	auto fileFindRes = mFileMap.find(file);
	if (fileFindRes != mFileMap.end())
	{
		auto lineFindRes = fileFindRes->second.find(line);
		if (lineFindRes != fileFindRes->second.end())
		{
			symbol = lineFindRes->second;
			return true;
		}
	}
	return false;
}

void Symbols::SetModuleBaseAddress(std::string moduleName, DWORD64 moduleBaseAddress)
{
	mModulesBaseAddresses[moduleName] = moduleBaseAddress;
}

DWORD64 Symbols::GetModuleBaseAddress(std::string moduleName)
{
	auto findRes = mModulesBaseAddresses.find(moduleName);
	if (findRes == mModulesBaseAddresses.end())
	{
		return NULL;
	}
	return findRes->second;
}

bool Symbols::GetSymbolFromAddress(ULONG64 address, SYMBOL_INFO *symbol)
{
	DWORD64 displacement;
	symbol->SizeOfStruct = sizeof(SYMBOL_INFO);
	symbol->MaxNameLen = MAX_SYM_NAME;
	WIN32_RETURNV(SymFromAddr(mProcessHandle, address, &displacement, symbol), false);
	return true;
}

bool Symbols::GetLineFromAddress(ULONG64 address, IMAGEHLP_LINE64 &line)
{
	DWORD displacement;
	line.SizeOfStruct = sizeof(IMAGEHLP_LINE64);
	WIN32_RETURNV(SymGetLineFromAddr64(mProcessHandle, address, &displacement, &line), false);
	return true;
}

size_t Symbols::GetLocalSymbols(CallstackEntry &callstackEntry, Symbol *buffer, size_t bufferSize)
{
    const size_t stackBytesRead = 0x400;

	BYTE *stackMemory = static_cast<BYTE *>(VirtualAlloc(NULL, sizeof(BYTE) * 0x8000, MEM_COMMIT, PAGE_READWRITE));
	if (!stackMemory)
	{
		BREAK_MSG("ERROR: Failed to allocate memory to read local symbols\n");
		return 0;
	}

	HANDLE thread = OpenThread(THREAD_ALL_ACCESS, false, callstackEntry.threadId);
	CONTEXT contextt = {};
	contextt.ContextFlags = CONTEXT_ALL;
	WIN32_ASSERT(GetThreadContext(thread, &contextt));
	CloseHandle(thread);

	void *stackAddress = (void *)callstackEntry.basePointer;
	if (!stackAddress)
		return 0;

	WIN32_ASSERT(ReadProcessMemory(mProcessHandle, stackAddress, stackMemory, sizeof(BYTE) * stackBytesRead, NULL));

	IMAGEHLP_STACK_FRAME stackFrame = {};
	stackFrame.InstructionOffset = callstackEntry.pcOffset;
	if (!SymSetContext(mProcessHandle, &stackFrame, NULL) && GetLastError() != ERROR_SUCCESS)
	{
		LOGF("Warning: SymSetContext failed with error 0x%x\n", GetLastError());
		return 0;
	}

	SymbolEnumContext context = { this, stackMemory, stackBytesRead, stackAddress, buffer, bufferSize, 0, 0 };
	WIN32_ASSERT(SymEnumSymbols(mProcessHandle, 0, 0, sEnumLocalSymbolsCallback, &context));

	VirtualFree(stackMemory, 0, MEM_RELEASE);
	return context.index;
}

void Symbols::ReadLocalSymbol(Symbol &symbol, const char *name, ULONG64 address, ULONG typeIndex, SymbolEnumContext &context)
{
	char *nameBuffer = static_cast<char *>(VirtualAlloc(NULL, sizeof(char) * MAX_SYMBOL_NAME_SIZE, MEM_COMMIT, PAGE_READWRITE));
	if (name)
	{
		Str::CopyStr(name, nameBuffer);
	}
	else
	{
		wchar_t *wname;
		SymGetTypeInfo(mProcessHandle, context.modBase, typeIndex, TI_GET_SYMNAME, &wname);
		Str::CopyStr(wname, nameBuffer);
		VirtualFree(wname, 0, MEM_RELEASE);
	}
	symbol.name = nameBuffer;

	DWORD tag;
	SymGetTypeInfo(mProcessHandle, context.modBase, typeIndex, TI_GET_SYMTAG, &tag);

	switch (tag)
	{
	case 0x0b: // SymTagUDT
	{
		UDTInfo *const udtInfo = new UDTInfo();
		symbol.basicType = BasicType::UDT;
		symbol.data.asUDTInfo = udtInfo;
		WIN32_ASSERT(SymGetTypeInfo(mProcessHandle, context.modBase, typeIndex, TI_GET_CHILDRENCOUNT, &udtInfo->childrenCount));

		// Get UDT name
		char *const udtNameBuffer = static_cast<char *>(VirtualAlloc(NULL, sizeof(char) * MAX_SYMBOL_NAME_SIZE, MEM_COMMIT, PAGE_READWRITE));
		GetSymbolName(typeIndex, udtNameBuffer, context);
		udtInfo->name = udtNameBuffer;

		size_t memSize = sizeof(TI_FINDCHILDREN_PARAMS) + sizeof(DWORD) * udtInfo->childrenCount;
		TI_FINDCHILDREN_PARAMS *childrenInfo = (TI_FINDCHILDREN_PARAMS *)VirtualAlloc(NULL, memSize, MEM_COMMIT, PAGE_READWRITE);
		childrenInfo->Count = udtInfo->childrenCount;
		WIN32_ASSERT(SymGetTypeInfo(mProcessHandle, context.modBase, typeIndex, TI_FINDCHILDREN, childrenInfo));

		udtInfo->children = new Symbol[childrenInfo->Count];
		ULONG64 childDataAddress = address + childrenInfo->Start;
		for (unsigned int childIdx = 0; childIdx < childrenInfo->Count; ++childIdx)
		{
			ReadLocalSymbol(udtInfo->children[childIdx], NULL, childDataAddress, childrenInfo->ChildId[childIdx], context);
			childDataAddress += udtInfo->children[childIdx].size;
		}

		VirtualFree(childrenInfo, 0, MEM_RELEASE);
	} break;
	case 0x07: // SymTagData
	{
		WIN32_ASSERT(SymGetTypeInfo(mProcessHandle, context.modBase, typeIndex, TI_GET_TYPEID, &typeIndex));
		FillBasicTypeInfo(symbol, typeIndex, context);
		FillValue(symbol, address, context);
	} break;
	case 0x0e: // SymTagPointerType
	{
		symbol.basicType = BasicType::Pointer;
		WIN32_ASSERT(SymGetTypeInfo(mProcessHandle, context.modBase, typeIndex, TI_GET_TYPEID, &typeIndex));

		// Retrieve pointed address
		symbol.size = sizeof(size_t);
		FillValue(symbol, address, context);

		// Construct pointer info struct
		const DWORD64 ptrAddress = symbol.data.bytes;
		Symbol *pointed = new Symbol();
		ReadLocalSymbol(*pointed, "_", ptrAddress, typeIndex, context);

		// Get pointed type name
		char *const typeNameBuffer = static_cast<char *>(VirtualAlloc(NULL, sizeof(char) * MAX_SYMBOL_NAME_SIZE, MEM_COMMIT, PAGE_READWRITE));
		GetSymbolName(typeIndex, typeNameBuffer, context);

		PointerInfo *pointerInfo = new PointerInfo { typeNameBuffer, ptrAddress, pointed };
		symbol.data.asPointerInfo = pointerInfo;
	} break;
	case 0x10: // SymTagBaseType
	{
		FillBasicTypeInfo(symbol, typeIndex, context);
		FillValue(symbol, address, context);
	} break;
	default:
	{
	} break;
	}
}

void Symbols::FillBasicTypeInfo(Symbol &symbol, ULONG typeIndex, SymbolEnumContext &context)
{
	SymGetTypeInfo(mProcessHandle, context.modBase, typeIndex, TI_GET_BASETYPE, &symbol.basicType);
	SymGetTypeInfo(mProcessHandle, context.modBase, typeIndex, TI_GET_LENGTH, &symbol.size);
}

void Symbols::FillValue(Symbol &symbol, ULONG64 address, SymbolEnumContext &context)
{
	BYTE *baseAddress = (BYTE *)address;
	DWORD64 baseTarget;
	size_t bytesRead;
	WIN32_WARN(ReadProcessMemory(mProcessHandle, baseAddress, &baseTarget, symbol.size, &bytesRead));
	symbol.data.bytes = baseTarget;
}

void Symbols::GetSymbolName(ULONG typeIndex, char *const nameBuffer, SymbolEnumContext &context)
{
	wchar_t *wname;
	if (SymGetTypeInfo(mProcessHandle, context.modBase, typeIndex, TI_GET_SYMNAME, &wname))
		Str::CopyStr(wname, nameBuffer);
	VirtualFree(wname, 0, MEM_RELEASE);
}
