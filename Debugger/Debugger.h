#pragma once
#pragma unmanaged

#include <windows.h>
#include <dbghelp.h>
#include <unordered_map>
#include <unordered_set>

#include "Breakpoints.h"
#include "Symbols.h"

const DWORD STACKWALK_MAX_NAMELEN = 1024;

/// <summary>
/// Struct to pass callstack information to UI
/// </summary>
struct CallstackEntry
{
	DWORD threadId;
	DWORD64 pcOffset;
	DWORD64 stackPointer;
	DWORD64 framePointer;
	DWORD64 basePointer;
	DWORD64 returnAddress;
	CHAR name[STACKWALK_MAX_NAMELEN];
	ULONG symbolId;
	DWORD lineNumber;
	CHAR lineFilename[STACKWALK_MAX_NAMELEN];
};

struct Symbol;

class Debugger
{
public:
	Debugger();
	~Debugger();

	// Static callback method
	friend static DWORD WINAPI sStart(LPVOID debugger);
	friend static BOOL CALLBACK sEnumLocalSymbolsCallback(SYMBOL_INFO *symbolInfo, ULONG symbolSize, void *stackMemory);

	void Start(const char *target);
	void Continue();
	void Pause();
	void StepInto();
	void StepOver();
	void StepOut();
	void SetReady(bool ready);
	void UpdateCallstack(HANDLE threadHandle);
	bool ToggleBreakpoint(const char *file, const int line);
	size_t GetLocalSymbols(unsigned int stackFrame, Symbol *buffer, size_t bufferSize);

	void SetOnNewCallstack(void(*onNewCallstack)(CallstackEntry *callstackEntries, DWORD callstackEntryCount));
	void SetOnOutputString(void(*onOutputString)(const char *message));

private:
	const char *mTarget;
	char *mApplicationPath = nullptr;
	HANDLE mProcessHandle = {};
	std::unordered_set<DWORD> mThreads;
	HANDLE mDebuggerThread;
	DWORD mDebuggerThreadId;
	CallstackEntry *mCurrentCallstack = nullptr;
	WORD mCallstackCount = 0;
	CallstackEntry mLastSymbol;
	bool mReady = false;
	bool mPaused = false;

	Symbols mSymbols;
	Breakpoints mBreakpoints;

	// Callback called at a breakpoint, with all info needed by UI
	void(*mOnNewCallstack)(CallstackEntry *callstackEntries, DWORD callstackEntryCount);
	void(*mOnOutputString)(const char *message);
	void(*mOnSingleStep)(Debugger* debugger, DEBUG_EVENT &debugEvent);
	void(*mOnBreakpoint)(Debugger* debugger, DEBUG_EVENT &debugEvent);

	void Init();
	void Loop();

	void HandleExceptionBreakpoint(DEBUG_EVENT &debugEvent);
	void HandleExceptionSingleStep(DEBUG_EVENT &debugEvent);
	void HandleExceptionDefault(DEBUG_EVENT &debugEvent);

	BOOL SetPrivilege(HANDLE token, LPCTSTR privilege, BOOL enablePrivilege);
};
