#include "Debugger.h"

#include <psapi.h>
#include <stdio.h>
#include <aclapi.h>

#include "Breakpoints.h"
#include "StringUtils.h"
#include "Symbols.h"
#include "Win32Utils.h"

static DWORD WINAPI sStart(LPVOID debugger)
{
	((Debugger*)debugger)->Init();
	((Debugger*)debugger)->Loop();
	return 0;
}

Debugger::Debugger()
{
	mApplicationPath = new char[MAX_PATH];
	mCurrentCallstack = static_cast<CallstackEntry*>(VirtualAlloc(NULL, sizeof(CallstackEntry) * 32, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE));
}

Debugger::~Debugger()
{
	delete[] mApplicationPath;
	VirtualFree(mCurrentCallstack, 0, MEM_RELEASE);
}

BOOL Debugger::SetPrivilege(HANDLE token, LPCTSTR privilege, BOOL enablePrivilege)
{
	TOKEN_PRIVILEGES tokenPrivileges;
	LUID luid;

	if (!LookupPrivilegeValue(NULL, privilege, &luid))
		return FALSE;

	tokenPrivileges.PrivilegeCount = 1;
	tokenPrivileges.Privileges[0].Luid = luid;
	if (enablePrivilege)
		tokenPrivileges.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	else
		tokenPrivileges.Privileges[0].Attributes = 0;

	if (!AdjustTokenPrivileges(token, FALSE, &tokenPrivileges, sizeof(TOKEN_PRIVILEGES), (PTOKEN_PRIVILEGES) NULL, (PDWORD) NULL))
		return FALSE;

	return TRUE;
}

void Debugger::UpdateCallstack(HANDLE threadHandle)
{
	CONTEXT threadContext = {};
	threadContext.ContextFlags = CONTEXT_FULL;

	WIN32_ASSERT(GetThreadContext(threadHandle, &threadContext));

	STACKFRAME64 stack = {};
	stack.AddrPC.Offset = threadContext.Rip;
	stack.AddrPC.Mode = AddrModeFlat;
	stack.AddrFrame.Offset = threadContext.Rbp;
	stack.AddrFrame.Mode = AddrModeFlat;
	stack.AddrStack.Offset = threadContext.Rsp;
	stack.AddrStack.Mode = AddrModeFlat;

	mCallstackCount = 0;
	for (int frameIdx = 0; frameIdx < 32; ++frameIdx)
	{
		if (!StackWalk64(IMAGE_FILE_MACHINE_AMD64, mProcessHandle, threadHandle,
			&stack, &threadContext, NULL, SymFunctionTableAccess64, SymGetModuleBase64, 0))
		{
			break;
		}

		CallstackEntry callstackEntry = {};
		callstackEntry.threadId = GetThreadId(threadHandle);
		callstackEntry.pcOffset = stack.AddrPC.Offset;
		callstackEntry.stackPointer = stack.AddrStack.Offset;
		callstackEntry.framePointer = stack.AddrFrame.Offset;
		callstackEntry.basePointer = threadContext.Rbp; // Sketchy af
		callstackEntry.returnAddress = stack.AddrReturn.Offset;

		char buffer[sizeof(SYMBOL_INFO) + MAX_SYM_NAME * sizeof(TCHAR)] = {};
		SYMBOL_INFO *symbol = (SYMBOL_INFO*)buffer;
		mSymbols.GetSymbolFromAddress(stack.AddrPC.Offset, symbol);

		callstackEntry.symbolId = symbol->Index;
		if (symbol->Name)
			Str::CopyStr(symbol->Name, callstackEntry.name);

		IMAGEHLP_LINE64 line = {};
		mSymbols.GetLineFromAddress(stack.AddrPC.Offset, line);

		callstackEntry.lineNumber = line.LineNumber;
		if (line.FileName)
			Str::CopyStr(line.FileName, callstackEntry.lineFilename);

		mCurrentCallstack[frameIdx] = callstackEntry;
		++mCallstackCount;
	}
}

void Debugger::SetOnNewCallstack(void (*onNewCallstack)(CallstackEntry *callstackEntries, DWORD callstackEntryCount))
{
	mOnNewCallstack = onNewCallstack;
}

void Debugger::SetOnOutputString(void (*onOutputString)(const char *message))
{
	mOnOutputString = onOutputString;
}

void Debugger::Start(const char *target)
{
	mTarget = target;
	Str::FilePathTruncateLast(mTarget, mApplicationPath);

	mDebuggerThread = CreateThread(NULL, 0, sStart, this, 0, &mDebuggerThreadId);
}

void Debugger::HandleExceptionBreakpoint(DEBUG_EVENT &debugEvent)
{
	const DWORD access = THREAD_GET_CONTEXT | THREAD_SET_CONTEXT | THREAD_SUSPEND_RESUME | THREAD_QUERY_INFORMATION;
	HANDLE threadHandle = OpenThread(access, false, debugEvent.dwThreadId);
	SuspendThread(threadHandle);
	mPaused = true;

	// Lazy symbols initialize
	if (!mSymbols.IsInitialized())
	{
		mSymbols.Initialize(mProcessHandle, mApplicationPath);
	}

	UpdateCallstack(threadHandle);
	ULONG64 programCounter = mCurrentCallstack[0].pcOffset;

	if (mOnBreakpoint)
		mOnBreakpoint(this, debugEvent);

	mBreakpoints.NotifyBreakpoint(programCounter, threadHandle, debugEvent);

	ResumeThread(threadHandle);
	CloseHandle(threadHandle);

	if (mPaused && mOnNewCallstack)
		mOnNewCallstack(mCurrentCallstack, mCallstackCount);

	while (mPaused)
	{
		Sleep(20);
	}
}

void Debugger::HandleExceptionSingleStep(DEBUG_EVENT &debugEvent)
{
	if (mOnSingleStep)
	{
		mOnSingleStep(this, debugEvent);
	}
}

void Debugger::HandleExceptionDefault(DEBUG_EVENT &debugEvent)
{
	HANDLE threadHandle = OpenThread(THREAD_GET_CONTEXT | THREAD_SET_CONTEXT | THREAD_SUSPEND_RESUME, false, debugEvent.dwThreadId);

	UpdateCallstack(threadHandle);
	if (mOnNewCallstack)
		mOnNewCallstack(mCurrentCallstack, mCallstackCount);
	DWORD exceptionCode = debugEvent.u.Exception.ExceptionRecord.ExceptionCode;
	BREAK_MSG("Exception hit: 0x%x\n", exceptionCode);
}

void Debugger::Init()
{
	// Set permitions
	HANDLE currentThreadToken;
	if (!OpenThreadToken(GetCurrentThread(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, FALSE, &currentThreadToken))
	{
		if (GetLastError() == ERROR_NO_TOKEN && ImpersonateSelf(SecurityImpersonation))
		{
			WIN32_WARN(OpenThreadToken(GetCurrentThread(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, FALSE, &currentThreadToken));
		}
	}
	WIN32_WARN(SetPrivilege(currentThreadToken, SE_DEBUG_NAME, true));
	CloseHandle(currentThreadToken);

	while (!mReady)
	{
		Sleep(20);
	}

	// Create debugee process
	STARTUPINFOA startupInfo = {};
	PROCESS_INFORMATION processInfo = {};
	WIN32_ASSERT(CreateProcessA(mTarget, NULL, NULL, NULL, false, DEBUG_PROCESS | DEBUG_ONLY_THIS_PROCESS, NULL, NULL, &startupInfo, &processInfo));
	mProcessHandle = processInfo.hProcess;
	if (startupInfo.hStdError) CloseHandle(startupInfo.hStdError);
	if (startupInfo.hStdInput) CloseHandle(startupInfo.hStdInput);
	if (startupInfo.hStdOutput) CloseHandle(startupInfo.hStdOutput);
	if (processInfo.hThread) CloseHandle(processInfo.hThread);

	// Wait for process to be ready
	DEBUG_EVENT debugEvent;
	while (true)
	{
		WaitForDebugEvent(&debugEvent, INFINITE);
		ContinueDebugEvent(debugEvent.dwProcessId, debugEvent.dwThreadId, DBG_CONTINUE);
		ASSERT(debugEvent.dwDebugEventCode == CREATE_PROCESS_DEBUG_EVENT);
		if (debugEvent.dwDebugEventCode == CREATE_PROCESS_DEBUG_EVENT)
		{
			break;
		}
	}

	const DWORD64 baseAddr = reinterpret_cast<DWORD64>(debugEvent.u.CreateProcessInfo.lpBaseOfImage);
	mBreakpoints.Initialize(mProcessHandle, baseAddr, &mSymbols);
}

void Debugger::Loop()
{
	bool running = true;
	while (running)
	{
		DEBUG_EVENT debugEvent;
		WaitForDebugEvent(&debugEvent, INFINITE);
		if (debugEvent.dwDebugEventCode == 0)
			continue;

		CONTEXT threadContext {};
		threadContext.ContextFlags = CONTEXT_CONTROL;

		switch (debugEvent.dwDebugEventCode)
		{
			case EXCEPTION_DEBUG_EVENT:
			{
				switch (debugEvent.u.Exception.ExceptionRecord.ExceptionCode)
				{
					case EXCEPTION_SINGLE_STEP:
					{
						HandleExceptionSingleStep(debugEvent);
					} break;
					case EXCEPTION_BREAKPOINT:
					{
						HandleExceptionBreakpoint(debugEvent);
					} break;
					default:
					{
						HandleExceptionDefault(debugEvent);
					} break;
				}
			} break;
			case LOAD_DLL_DEBUG_EVENT:
			{
				LOAD_DLL_DEBUG_INFO loadedDll = debugEvent.u.LoadDll;
				if (loadedDll.lpImageName)
				{
					// Get DLL name
					char imageName[MAX_PATH];
					GetFinalPathNameByHandleA(loadedDll.hFile, imageName, MAX_PATH, FILE_NAME_NORMALIZED);
					LOGF("Program loaded DLL %s\n", imageName);
					// Try to load symbols
					mSymbols.LoadModule(imageName, loadedDll.hFile);
					// Close DLL handle
					CloseHandle(loadedDll.hFile);
				}
			} break;
			case UNLOAD_DLL_DEBUG_EVENT:
			{
			} break;
			case CREATE_THREAD_DEBUG_EVENT:
			{
				HANDLE threadHandle = debugEvent.u.CreateThread.hThread;
				if (threadHandle)
				{
					CloseHandle(threadHandle);
				}
				DWORD threadId = debugEvent.dwThreadId;
				ASSERT(threadId);
				mThreads.insert(threadId);
			} break;
			case CREATE_PROCESS_DEBUG_EVENT:
			{
#ifdef DEBUG
				char buffer[MAX_PATH];
				GetModuleFileNameExA(debugEvent.u.CreateProcessInfo.hProcess, NULL, buffer, MAX_PATH);
				LOGF("Opening process \"%s\"\n", buffer);
				BREAK_MSG("Debugging multiple processes not currently supported\n");
#endif
			} break;
			case EXIT_THREAD_DEBUG_EVENT:
			{
				const DWORD threadId = debugEvent.dwThreadId;
				ASSERT(threadId);
				mThreads.erase(threadId);
			} break;
			case EXIT_PROCESS_DEBUG_EVENT:
			{
				if (debugEvent.dwProcessId == GetProcessId(mProcessHandle))
				{
					BREAK();
					running = false;
				}
			} break;
			case OUTPUT_DEBUG_STRING_EVENT:
			{
				OUTPUT_DEBUG_STRING_INFO dbgStr = debugEvent.u.DebugString;

				HANDLE process = OpenProcess(PROCESS_VM_READ, false, debugEvent.dwProcessId);
				char buffer[512];
				WIN32_ASSERT(ReadProcessMemory(process, dbgStr.lpDebugStringData, buffer, dbgStr.nDebugStringLength, NULL));

				if (mOnOutputString)
					mOnOutputString(buffer);

				CloseHandle(process);
			} break;
			default:
				BREAK();
				break;
		}
		ContinueDebugEvent(debugEvent.dwProcessId, debugEvent.dwThreadId, DBG_CONTINUE);
	}

	TerminateProcess(mProcessHandle, 0);
	CloseHandle(mProcessHandle);
}

void Debugger::Continue()
{
	mPaused = false;
}

void Debugger::Pause()
{
	// TODO: destroy all of these breakpoints upon hitting one (does a breakpoint freeze all threads?)
	for (DWORD threadId : mThreads)
	{
		HANDLE thread = OpenThread(THREAD_ALL_ACCESS, FALSE, threadId);
		if (thread == 0)
		{
			return;
		}

		SuspendThread(thread);

		CONTEXT threadContext{};
		threadContext.ContextFlags = CONTEXT_CONTROL;
		if (GetThreadContext(thread, &threadContext))
		{
			const DWORD64 address = threadContext.Rip;
			mBreakpoints.RegisterBreakpoint(address, false);
		}

		ResumeThread(thread);
		CloseHandle(thread);
	}
}

void Debugger::StepInto()
{
	if (!mPaused)
	{
		return;
	}

	// Stop on next instruction
	HANDLE threadHandle = OpenThread(THREAD_GET_CONTEXT | THREAD_SET_CONTEXT, 0, mCurrentCallstack[0].threadId);

	CONTEXT threadContext{};
	threadContext.ContextFlags = CONTEXT_CONTROL;
	WIN32_ASSERT(GetThreadContext(threadHandle, &threadContext));
	threadContext.EFlags |= 0x100;
	WIN32_ASSERT(SetThreadContext(threadHandle, &threadContext));

	CloseHandle(threadHandle);

	// Check if we're standing on a new symbol. Keep going if not
	mOnSingleStep = [](Debugger *debugger, DEBUG_EVENT &debugEvent)
	{
		bool foundNextLine = true;

		HANDLE threadHandle = OpenThread(THREAD_GET_CONTEXT | THREAD_SET_CONTEXT, 0, debugEvent.dwThreadId);
		CONTEXT threadContext = {};
		threadContext.ContextFlags = CONTEXT_CONTROL;
		WIN32_ASSERT(GetThreadContext(threadHandle, &threadContext));

		IMAGEHLP_LINE64 lineSymbol = {};
		if (!debugger->mSymbols.GetLineFromAddress(threadContext.Rip, lineSymbol))
		{
			// Keep going if there's no loaded symbol. Otherwise we have to break on
			// every instruction of system calls and such.
			foundNextLine = false;
		}
		// These seems to be caused by JIT
		else if (lineSymbol.LineNumber == 0x00f00f00)
		{
			LOGF("Hit 0xf00f00 again on symbol %s (0x%p)", lineSymbol.FileName, (void *)lineSymbol.Address);
			foundNextLine = false;
		}
		else
		{
			const char *lastLineFile = debugger->mCurrentCallstack[0].lineFilename;
			int lastLineNumber = debugger->mCurrentCallstack[0].lineNumber;
			if (lineSymbol.LineNumber == lastLineNumber && strcmp(lastLineFile, lineSymbol.FileName) == 0)
			{
				foundNextLine = false;
			}
		}

		if (foundNextLine)
		{
			// Break
			debugger->mOnSingleStep = nullptr;
			debugger->mBreakpoints.RegisterBreakpoint(threadContext.Rip, false);
		}
		else
		{
			// Keep going
			threadContext.EFlags |= 0x100;
			WIN32_ASSERT(SetThreadContext(threadHandle, &threadContext));
		}
		CloseHandle(threadHandle);
	};
	Continue();
}

void Debugger::StepOut()
{
	ASSERT(mCurrentCallstack != nullptr);
	if (!mPaused || !mCurrentCallstack)
	{
		return;
	}

	HANDLE threadHandle = OpenThread(THREAD_GET_CONTEXT, 0, mCurrentCallstack[0].threadId);
	ASSERT(threadHandle);
	if (threadHandle)
	{
		CONTEXT threadContext{};
		threadContext.ContextFlags = CONTEXT_CONTROL;
		WIN32_ASSERT(GetThreadContext(threadHandle, &threadContext));
		CloseHandle(threadHandle);
	}

	const DWORD64 returnAddress = mCurrentCallstack[0].returnAddress;
	if (returnAddress)
	{
		mBreakpoints.RegisterBreakpoint(mCurrentCallstack[0].returnAddress, false);
		Continue();
	}
}

void Debugger::StepOver()
{
	if (!mPaused || !mCurrentCallstack)
	{
		return;
	}
	mLastSymbol = mCurrentCallstack[0];
	StepInto();

	mOnBreakpoint = [](Debugger *debugger, DEBUG_EVENT &debugEvent)
	{
		if (debugger->mLastSymbol.symbolId != debugger->mCurrentCallstack[0].symbolId &&
			debugger->mLastSymbol.stackPointer > debugger->mCurrentCallstack[0].stackPointer)
		{
			debugger->StepOut();
			debugger->mOnBreakpoint = [](Debugger *debugger, DEBUG_EVENT &debugEvent)
			{
				// If still on same line, repeat
				if (debugger->mCurrentCallstack[0].symbolId == debugger->mLastSymbol.symbolId &&
					debugger->mCurrentCallstack[0].lineNumber == debugger->mLastSymbol.lineNumber)
					debugger->StepOver();
				else
					debugger->mOnBreakpoint = nullptr;
			};
		}
		else
		{
			debugger->mOnBreakpoint = nullptr;
		}
	};
}

void Debugger::SetReady(bool ready)
{
	mReady = ready;
}

bool Debugger::ToggleBreakpoint(const char *file, const int line)
{
	DWORD64 bp = mBreakpoints.Find(file, line);
	if (bp)
	{
		bool couldUnregister = mBreakpoints.UnregisterBreakpoint(bp);
		return !couldUnregister;
	}
	else
	{
		bp = mBreakpoints.RegisterBreakpoint(file, line, true);
		return bp != NULL;
	}
}

size_t Debugger::GetLocalSymbols(unsigned int stackFrame, Symbol *buffer, size_t bufferSize)
{
	if (!mSymbols.IsInitialized())
		return 0;

	const size_t count = mSymbols.GetLocalSymbols(mCurrentCallstack[stackFrame], buffer, bufferSize);
	return count;
}
