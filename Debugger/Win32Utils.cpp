#include "Win32Utils.h"
#include "Debugger.h"

#ifdef DEBUG
void Win32PrintError(const char *funcName, const char *file, const int line, ...)
{
	char buffer[STACKWALK_MAX_NAMELEN];
	sprintf_s(buffer, STACKWALK_MAX_NAMELEN, "ERROR 0x%x on \"%s\" on %s : %d\n", GetLastError(), funcName, file, line);

	va_list argptr;
	va_start(argptr, buffer);
	vprintf_s(buffer, argptr);
	va_end(argptr);

	OutputDebugStringA(buffer);
}

DWORD Win32WriteConsole(const char *message)
{
	DWORD written = 0;
	HANDLE stdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	if (stdOut != NULL && stdOut != INVALID_HANDLE_VALUE)
	{
		DWORD count = 0;
		for (const char *scan = message; *scan; ++scan)
			++count;
		WriteConsoleA(stdOut, message, count, &written, NULL);
	}
	return written;
}
#else
void Win32PrintError(const char *funcName, const char *file, const int line, ...) {}
DWORD Win32WriteConsole(const char *message) { return 0; }
#endif