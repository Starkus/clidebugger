#include "StringUtils.h"
#include <stdio.h>
#include <string>

void Str::FromInt(long n, char *buffer, size_t size)
{
	sprintf_s(buffer, size, "%d", n);
}
void Str::FromFloat(double n, char *buffer, size_t size)
{
	sprintf_s(buffer, size, "%f", n);
}

int Str::Cmp(const char *a, const char *b)
{
	return strcmp(a, b);
}
int Str::CmpNoCase(const char *a, const char *b)
{
	char tmpA, tmpB;
	for (const char *scanA = a, *scanB = b; *scanA && *scanB; ++scanA, ++scanB)
	{
		tmpA = *scanA;
		tmpB = *scanB;

		if (tmpA == tmpB)
			continue;

		if (tmpA >= 'A' && tmpA <= 'Z')
			tmpA += 'a' - 'A';
		if (tmpB >= 'A' && tmpB <= 'Z')
			tmpB += 'a' - 'A';
		if (tmpA == '\\')
			tmpA = '/';
		if (tmpB == '\\')
			tmpB = '/';

		if (tmpA == tmpB)
			continue;
		else
			return tmpA > tmpB ? 1 : -1;
	}
	return 0;
}
bool Str::ArePathsEqual(const char *a, const char *b)
{
	// TODO - revisit
	for (const char *scanA = a, *scanB = b; *scanA && *scanB; ++scanA, ++scanB)
	{
		if (*scanA == *scanB)
			continue;
		if ((*scanA == '\\' || *scanA == '/') && (*scanB == '\\' || *scanB == '/'))
			continue;
		if ((*scanA >= 'A' && *scanA <= 'Z') && *scanA + ('a' - 'A') == *scanB)
			continue;
		if ((*scanB >= 'A' && *scanB <= 'Z') && *scanB + ('a' - 'A') == *scanA)
			continue;
		return false;
	}
	return true;
}

void Str::FilePathTruncateLast(const char *source, char *dest)
{
	const char *lastSlash = source;
	for (const char *scan = source; *scan; ++scan)
	{
		if (*scan == '\\' && *(scan + 1))
		{
			lastSlash = scan;
		}
	}
	CopyStr(source, ++lastSlash, dest);
}

void Str::CopyStr(const char *source, char *dest)
{
	char *writePtr = dest;
	for (const char *scan = source; *scan; ++scan)
	{
		*(writePtr++) = *scan;
	}
	*writePtr = 0;
}

void Str::CopyStr(const char *begin, const char *end, char *dest)
{
	char *writePtr = dest;
	for (const char *scan = begin; scan != end; ++scan)
	{
		*(writePtr++) = *scan;
	}
	*writePtr = 0;
}

void Str::CopyStr(const wchar_t *source, char *dest)
{
	char *writePtr = dest;
	for (const wchar_t *scan = source; *scan; ++scan)
	{
		*(writePtr++) = *(char *)scan;
	}
	*writePtr = 0;
}

