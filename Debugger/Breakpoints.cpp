#include "Breakpoints.h"

#include "Win32Utils.h"
#include "Symbols.h"

void Breakpoints::Initialize(HANDLE processHandle, DWORD64 baseOfImage, Symbols *symbols)
{
	mProcessHandle = processHandle;
	mBaseOfImage = baseOfImage;
	mSymbols = symbols;
}

DWORD64 Breakpoints::RegisterBreakpoint(const char *file, const int line, bool persist)
{
	if (file == NULL || line <= 0)
	{
		return 0;
	}

	DWORD64 address = GetAddressFromFileLine(file, line);
	if (address == NULL)
	{
		BREAK_MSG("Error setting up breakpoint in file %s, line %d\n", file, line);
		return 0;
	}

	return RegisterBreakpoint(address, persist);
}

DWORD64 Breakpoints::RegisterBreakpoint(DWORD64 address, bool persist)
{
	auto bpFindRes = mBreakpointsByAddress.find(address);
	if (bpFindRes != mBreakpointsByAddress.end())
	{
		Breakpoint &bpFound = bpFindRes->second;
		if (persist)
			bpFound.mPersist = true;
		return bpFound.mAddress;
	}

	Breakpoint breakpoint = {};
	breakpoint.mAddress = address;
	breakpoint.mPersist = persist;

	// Save original memory
	WIN32_ASSERT(ReadProcessMemory(mProcessHandle, (void *)address, &breakpoint.mReplacedMemory, sizeof(BYTE), NULL));

	// Replace for break instruction
	const BYTE breakInstruction = 0xcc;
	WIN32_ASSERT(WriteProcessMemory(mProcessHandle, (void *)address, &breakInstruction, sizeof(BYTE), NULL));

	WIN32_WARN(FlushInstructionCache(mProcessHandle, (void *)address, 1));

	mBreakpointsByAddress[address] = breakpoint;

	return address;
}

bool Breakpoints::UnregisterBreakpoint(DWORD64 address)
{
	auto bpFindRes = mBreakpointsByAddress.find(address);
	if (bpFindRes == mBreakpointsByAddress.end())
	{
		return false;
	}
	Breakpoint &breakpoint = bpFindRes->second;

#ifdef _DEBUG
	BYTE debugBuffer;
	WIN32_ASSERT(ReadProcessMemory(mProcessHandle, (void *)address, &debugBuffer, sizeof(BYTE), NULL));
	ASSERT(debugBuffer == 0xCC);
#endif

	WIN32_ASSERT(WriteProcessMemory(mProcessHandle, (void *)address, &breakpoint.mReplacedMemory, sizeof(char), NULL));

	WIN32_WARN(FlushInstructionCache(mProcessHandle, (void *)address, 1));

	mBreakpointsByAddress.erase(address);
	return true;
}

DWORD64 Breakpoints::GetAddressFromFileLine(const char *file, const int line)
{
	SRCCODEINFO symbol = {};
	DWORD64 address = 0;
	if (mSymbols->GetSymbolInFileLine(file, line, symbol))
		address = symbol.Address - symbol.ModBase + mBaseOfImage;

	return address;
}

DWORD64 Breakpoints::Find(const char *file, const int line)
{
	DWORD64 address = GetAddressFromFileLine(file, line);
	if (address)
	{
		auto findRes = mBreakpointsByAddress.find(address);
		if (findRes != mBreakpointsByAddress.end())
		{
			return findRes->second.mAddress;
		}
	}
	return NULL;
}

bool Breakpoints::IsActive(DWORD64 address)
{
	auto findRes = mBreakpointsByAddress.find(address);
	if (findRes != mBreakpointsByAddress.end())
	{
		return true;
	}
	return false;
}

bool Breakpoints::NotifyBreakpoint(ULONG64 programCounter, HANDLE thread, DEBUG_EVENT &debugEvent)
{
	// The program counter is after the 0xCC instruction, subtract
	ULONG64 breakpointAddress = programCounter - 1;
	auto breakpointFindRes = mBreakpointsByAddress.find(breakpointAddress);
	if (breakpointFindRes == mBreakpointsByAddress.end())
	{
		return false;
	}
	Breakpoint &breakpoint = breakpointFindRes->second;
	const bool shouldRestoreBreakpoint = breakpoint.mPersist;

	UnregisterBreakpoint(breakpointAddress);

	// Go back before the 0xCC instruction
	CONTEXT threadContext = {};
	threadContext.ContextFlags = CONTEXT_CONTROL;
	WIN32_ASSERT(GetThreadContext(thread, &threadContext));
	--threadContext.Rip;
	ASSERT(threadContext.Rip == breakpointAddress);
	threadContext.EFlags |= 0x100;
	WIN32_ASSERT(SetThreadContext(thread, &threadContext));

	// Restore breakpoint
	if (shouldRestoreBreakpoint)
	{
		ResumeThread(thread);
		ContinueDebugEvent(debugEvent.dwProcessId, debugEvent.dwThreadId, DBG_CONTINUE);
		do {
			WaitForDebugEvent(&debugEvent, INFINITE);
			ASSERT(debugEvent.dwDebugEventCode == EXCEPTION_DEBUG_EVENT);
		} while (debugEvent.u.Exception.ExceptionRecord.ExceptionCode != EXCEPTION_SINGLE_STEP);
		ASSERT(debugEvent.u.Exception.ExceptionRecord.ExceptionCode == EXCEPTION_SINGLE_STEP);
		SuspendThread(thread);

		const bool restored = RegisterBreakpoint(breakpointAddress, true);
		ASSERT(restored);
	}

	return true;
}