#pragma once

#include <Windows.h>

// Debugger forward declaration
class Debugger;

namespace CPPCLITest
{
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		static MyForm ^formInstance;

		MyForm(String ^targetExecutable)
		{
			mTargetExecutable = targetExecutable;
			InitializeComponent();
		}

		void ClearCallstack();
		void ClearLocalSymbols();
		void AddCallstackEntry(array<System::Object ^> ^row);
		void AddLocalSymbol(array<System::Object ^> ^row);
		void AppendTextToOutputBox(String ^message);
		bool OpenFile(System::String ^filename);

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}

		System::Void OnKeyDown(System::Object ^sender, System::Windows::Forms::KeyEventArgs ^e);

		String ^mTargetExecutable;

		String ^mCurrentFile;

	private: System::Windows::Forms::RichTextBox ^codeTextBox;
	private: System::Windows::Forms::DataGridView ^codeTable;
	private: System::ComponentModel::BackgroundWorker ^backgroundWorker1;
	private: System::Windows::Forms::DataGridView ^callstackTable;
	private: System::Windows::Forms::SplitContainer ^splitContainer1;
	private: System::Windows::Forms::SplitContainer ^splitContainer2;
	private: System::Windows::Forms::DataGridView ^symbolsTable;
	private: System::Windows::Forms::DataGridViewTextBoxColumn ^dataGridViewTextBoxColumn1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn ^dataGridViewTextBoxColumn2;
	private: System::Windows::Forms::ToolStrip ^toolStrip1;
	private: System::Windows::Forms::ToolStripButton ^continueButton;
	private: System::Windows::Forms::ToolStripButton ^pauseButton;
	private: System::Windows::Forms::ToolStripButton ^stepOverButton;
	private: System::Windows::Forms::ToolStripButton ^stepIntoButton;
	private: System::Windows::Forms::ToolStripButton ^stepOutButton;
	private: System::Windows::Forms::DataGridViewCheckBoxColumn ^codeColumnBreakpoints;
	private: System::Windows::Forms::DataGridViewTextBoxColumn ^codeColumnLine;
	private: System::Windows::Forms::DataGridViewTextBoxColumn ^codeColumnText;
	private: System::Windows::Forms::SplitContainer ^splitContainer3;
	private: System::Windows::Forms::DataGridViewTextBoxColumn ^callstackColumnSymbol;
	private: System::Windows::Forms::DataGridViewTextBoxColumn ^callstackColumnFile;
	private: System::Windows::Forms::DataGridViewTextBoxColumn ^callstackColumnLine;

	private: System::Windows::Forms::TextBox ^outputBox;
	private: System::Void OnFormShown(System::Object ^sender, System::EventArgs ^e);
	private: System::Void continueButton_Click(System::Object ^sender, System::EventArgs ^e);
	private: System::Void pauseButton_Click(System::Object ^sender, System::EventArgs ^e);
	private: System::Void stepOverButton_Click(System::Object ^sender, System::EventArgs ^e);
	private: System::Void stepIntoButton_Click(System::Object ^sender, System::EventArgs ^e);
	private: System::Void stepOutButton_Click(System::Object ^sender, System::EventArgs ^e);
	private: System::Void codeTable_MouseDown(System::Object ^sender, System::Windows::Forms::MouseEventArgs ^e);
	private: System::Void callstackTable_SelectionChanged(System::Object ^sender, System::EventArgs ^e);

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::Windows::Forms::DataGridViewCellStyle ^dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::ComponentModel::ComponentResourceManager ^resources = (gcnew System::ComponentModel::ComponentResourceManager(MyForm::typeid));
			this->codeTextBox = (gcnew System::Windows::Forms::RichTextBox());
			this->codeTable = (gcnew System::Windows::Forms::DataGridView());
			this->codeColumnBreakpoints = (gcnew System::Windows::Forms::DataGridViewCheckBoxColumn());
			this->codeColumnLine = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->codeColumnText = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
			this->callstackTable = (gcnew System::Windows::Forms::DataGridView());
			this->splitContainer1 = (gcnew System::Windows::Forms::SplitContainer());
			this->splitContainer3 = (gcnew System::Windows::Forms::SplitContainer());
			this->outputBox = (gcnew System::Windows::Forms::TextBox());
			this->splitContainer2 = (gcnew System::Windows::Forms::SplitContainer());
			this->symbolsTable = (gcnew System::Windows::Forms::DataGridView());
			this->dataGridViewTextBoxColumn1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->dataGridViewTextBoxColumn2 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
			this->continueButton = (gcnew System::Windows::Forms::ToolStripButton());
			this->pauseButton = (gcnew System::Windows::Forms::ToolStripButton());
			this->stepOverButton = (gcnew System::Windows::Forms::ToolStripButton());
			this->stepIntoButton = (gcnew System::Windows::Forms::ToolStripButton());
			this->stepOutButton = (gcnew System::Windows::Forms::ToolStripButton());
			this->callstackColumnSymbol = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->callstackColumnFile = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->callstackColumnLine = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize ^>(this->codeTable))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize ^>(this->callstackTable))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize ^>(this->splitContainer1))->BeginInit();
			this->splitContainer1->Panel1->SuspendLayout();
			this->splitContainer1->Panel2->SuspendLayout();
			this->splitContainer1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize ^>(this->splitContainer3))->BeginInit();
			this->splitContainer3->Panel1->SuspendLayout();
			this->splitContainer3->Panel2->SuspendLayout();
			this->splitContainer3->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize ^>(this->splitContainer2))->BeginInit();
			this->splitContainer2->Panel1->SuspendLayout();
			this->splitContainer2->Panel2->SuspendLayout();
			this->splitContainer2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize ^>(this->symbolsTable))->BeginInit();
			this->toolStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// codeTextBox
			// 
			this->codeTextBox->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->codeTextBox->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(24)), static_cast<System::Int32>(static_cast<System::Byte>(24)),
				static_cast<System::Int32>(static_cast<System::Byte>(24)));
			this->codeTextBox->Font = (gcnew System::Drawing::Font(L"Consolas", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->codeTextBox->ForeColor = System::Drawing::Color::Gainsboro;
			this->codeTextBox->Location = System::Drawing::Point(12, 12);
			this->codeTextBox->Name = L"codeTextBox";
			this->codeTextBox->Size = System::Drawing::Size(484, 538);
			this->codeTextBox->TabIndex = 0;
			this->codeTextBox->Text = L"";
			// 
			// codeTable
			// 
			this->codeTable->AllowUserToAddRows = false;
			this->codeTable->AllowUserToDeleteRows = false;
			this->codeTable->AllowUserToResizeRows = false;
			this->codeTable->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCellsExceptHeader;
			this->codeTable->CellBorderStyle = System::Windows::Forms::DataGridViewCellBorderStyle::None;
			this->codeTable->ColumnHeadersVisible = false;
			this->codeTable->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn ^  >(3)
			{
				this->codeColumnBreakpoints,
					this->codeColumnLine, this->codeColumnText
			});
			dataGridViewCellStyle2->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
			dataGridViewCellStyle2->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(24)), static_cast<System::Int32>(static_cast<System::Byte>(24)),
				static_cast<System::Int32>(static_cast<System::Byte>(24)));
			dataGridViewCellStyle2->Font = (gcnew System::Drawing::Font(L"Consolas", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			dataGridViewCellStyle2->ForeColor = System::Drawing::Color::Gainsboro;
			dataGridViewCellStyle2->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle2->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle2->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
			this->codeTable->DefaultCellStyle = dataGridViewCellStyle2;
			this->codeTable->Dock = System::Windows::Forms::DockStyle::Fill;
			this->codeTable->Font = (gcnew System::Drawing::Font(L"Consolas", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->codeTable->Location = System::Drawing::Point(0, 0);
			this->codeTable->MultiSelect = false;
			this->codeTable->Name = L"codeTable";
			this->codeTable->ReadOnly = true;
			this->codeTable->RowHeadersVisible = false;
			this->codeTable->RowTemplate->Height = 18;
			this->codeTable->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->codeTable->Size = System::Drawing::Size(729, 503);
			this->codeTable->TabIndex = 5;
			this->codeTable->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::codeTable_MouseDown);
			// 
			// codeColumnBreakpoints
			// 
			this->codeColumnBreakpoints->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
			this->codeColumnBreakpoints->HeaderText = L"Breakpoints";
			this->codeColumnBreakpoints->Name = L"codeColumnBreakpoints";
			this->codeColumnBreakpoints->ReadOnly = true;
			this->codeColumnBreakpoints->Resizable = System::Windows::Forms::DataGridViewTriState::True;
			this->codeColumnBreakpoints->Width = 42;
			// 
			// codeColumnLine
			// 
			this->codeColumnLine->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
			this->codeColumnLine->HeaderText = L"Line";
			this->codeColumnLine->Name = L"codeColumnLine";
			this->codeColumnLine->ReadOnly = true;
			this->codeColumnLine->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			this->codeColumnLine->Width = 5;
			// 
			// codeColumnText
			// 
			this->codeColumnText->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
			this->codeColumnText->HeaderText = L"Code";
			this->codeColumnText->Name = L"codeColumnText";
			this->codeColumnText->ReadOnly = true;
			this->codeColumnText->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// callstackTable
			// 
			this->callstackTable->AllowUserToAddRows = false;
			this->callstackTable->AllowUserToDeleteRows = false;
			this->callstackTable->AllowUserToResizeRows = false;
			this->callstackTable->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->callstackTable->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->callstackTable->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn ^  >(3)
			{
				this->callstackColumnSymbol,
					this->callstackColumnFile, this->callstackColumnLine
			});
			this->callstackTable->Dock = System::Windows::Forms::DockStyle::Fill;
			this->callstackTable->Location = System::Drawing::Point(0, 0);
			this->callstackTable->MultiSelect = false;
			this->callstackTable->Name = L"callstackTable";
			this->callstackTable->ReadOnly = true;
			this->callstackTable->RowHeadersVisible = false;
			this->callstackTable->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->callstackTable->Size = System::Drawing::Size(406, 341);
			this->callstackTable->TabIndex = 2;
			this->callstackTable->SelectionChanged += gcnew System::EventHandler(this, &MyForm::callstackTable_SelectionChanged);
			// 
			// splitContainer1
			// 
			this->splitContainer1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->splitContainer1->Location = System::Drawing::Point(9, 37);
			this->splitContainer1->Name = L"splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this->splitContainer1->Panel1->Controls->Add(this->splitContainer3);
			// 
			// splitContainer1.Panel2
			// 
			this->splitContainer1->Panel2->Controls->Add(this->splitContainer2);
			this->splitContainer1->Size = System::Drawing::Size(1139, 683);
			this->splitContainer1->SplitterDistance = 729;
			this->splitContainer1->TabIndex = 6;
			// 
			// splitContainer3
			// 
			this->splitContainer3->Dock = System::Windows::Forms::DockStyle::Fill;
			this->splitContainer3->Location = System::Drawing::Point(0, 0);
			this->splitContainer3->Name = L"splitContainer3";
			this->splitContainer3->Orientation = System::Windows::Forms::Orientation::Horizontal;
			// 
			// splitContainer3.Panel1
			// 
			this->splitContainer3->Panel1->Controls->Add(this->codeTable);
			// 
			// splitContainer3.Panel2
			// 
			this->splitContainer3->Panel2->Controls->Add(this->outputBox);
			this->splitContainer3->Size = System::Drawing::Size(729, 683);
			this->splitContainer3->SplitterDistance = 503;
			this->splitContainer3->TabIndex = 6;
			// 
			// outputBox
			// 
			this->outputBox->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(24)), static_cast<System::Int32>(static_cast<System::Byte>(24)),
				static_cast<System::Int32>(static_cast<System::Byte>(24)));
			this->outputBox->Dock = System::Windows::Forms::DockStyle::Fill;
			this->outputBox->Font = (gcnew System::Drawing::Font(L"Consolas", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->outputBox->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(224)), static_cast<System::Int32>(static_cast<System::Byte>(224)),
				static_cast<System::Int32>(static_cast<System::Byte>(224)));
			this->outputBox->Location = System::Drawing::Point(0, 0);
			this->outputBox->Multiline = true;
			this->outputBox->Name = L"outputBox";
			this->outputBox->ReadOnly = true;
			this->outputBox->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
			this->outputBox->Size = System::Drawing::Size(729, 176);
			this->outputBox->TabIndex = 0;
			// 
			// splitContainer2
			// 
			this->splitContainer2->Dock = System::Windows::Forms::DockStyle::Fill;
			this->splitContainer2->Location = System::Drawing::Point(0, 0);
			this->splitContainer2->Name = L"splitContainer2";
			this->splitContainer2->Orientation = System::Windows::Forms::Orientation::Horizontal;
			// 
			// splitContainer2.Panel1
			// 
			this->splitContainer2->Panel1->Controls->Add(this->callstackTable);
			// 
			// splitContainer2.Panel2
			// 
			this->splitContainer2->Panel2->Controls->Add(this->symbolsTable);
			this->splitContainer2->Size = System::Drawing::Size(406, 683);
			this->splitContainer2->SplitterDistance = 341;
			this->splitContainer2->TabIndex = 3;
			// 
			// symbolsTable
			// 
			this->symbolsTable->AllowUserToAddRows = false;
			this->symbolsTable->AllowUserToDeleteRows = false;
			this->symbolsTable->AllowUserToResizeRows = false;
			this->symbolsTable->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->symbolsTable->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->symbolsTable->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn ^  >(2)
			{
				this->dataGridViewTextBoxColumn1,
					this->dataGridViewTextBoxColumn2
			});
			this->symbolsTable->Dock = System::Windows::Forms::DockStyle::Fill;
			this->symbolsTable->Location = System::Drawing::Point(0, 0);
			this->symbolsTable->MultiSelect = false;
			this->symbolsTable->Name = L"symbolsTable";
			this->symbolsTable->ReadOnly = true;
			this->symbolsTable->RowHeadersVisible = false;
			this->symbolsTable->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->symbolsTable->Size = System::Drawing::Size(406, 338);
			this->symbolsTable->TabIndex = 3;
			// 
			// dataGridViewTextBoxColumn1
			// 
			this->dataGridViewTextBoxColumn1->HeaderText = L"Symbol";
			this->dataGridViewTextBoxColumn1->Name = L"dataGridViewTextBoxColumn1";
			this->dataGridViewTextBoxColumn1->ReadOnly = true;
			this->dataGridViewTextBoxColumn1->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// dataGridViewTextBoxColumn2
			// 
			this->dataGridViewTextBoxColumn2->HeaderText = L"Content";
			this->dataGridViewTextBoxColumn2->Name = L"dataGridViewTextBoxColumn2";
			this->dataGridViewTextBoxColumn2->ReadOnly = true;
			this->dataGridViewTextBoxColumn2->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// toolStrip1
			// 
			this->toolStrip1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->toolStrip1->AutoSize = false;
			this->toolStrip1->Dock = System::Windows::Forms::DockStyle::None;
			this->toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem ^  >(5)
			{
				this->continueButton, this->pauseButton,
					this->stepOverButton, this->stepIntoButton, this->stepOutButton
			});
			this->toolStrip1->Location = System::Drawing::Point(9, 9);
			this->toolStrip1->Name = L"toolStrip1";
			this->toolStrip1->Size = System::Drawing::Size(1139, 25);
			this->toolStrip1->TabIndex = 7;
			this->toolStrip1->Text = L"toolStrip1";
			// 
			// continueButton
			// 
			this->continueButton->Image = (cli::safe_cast<System::Drawing::Image ^>(resources->GetObject(L"continueButton.Image")));
			this->continueButton->ImageAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->continueButton->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->continueButton->Name = L"continueButton";
			this->continueButton->Size = System::Drawing::Size(76, 22);
			this->continueButton->Text = L"Continue";
			this->continueButton->Click += gcnew System::EventHandler(this, &MyForm::continueButton_Click);
			// 
			// pauseButton
			// 
			this->pauseButton->Image = (cli::safe_cast<System::Drawing::Image ^>(resources->GetObject(L"pauseButton.Image")));
			this->pauseButton->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->pauseButton->Name = L"pauseButton";
			this->pauseButton->Size = System::Drawing::Size(58, 22);
			this->pauseButton->Text = L"Pause";
			this->pauseButton->Click += gcnew System::EventHandler(this, &MyForm::pauseButton_Click);
			// 
			// stepOverButton
			// 
			this->stepOverButton->Image = (cli::safe_cast<System::Drawing::Image ^>(resources->GetObject(L"stepOverButton.Image")));
			this->stepOverButton->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->stepOverButton->Name = L"stepOverButton";
			this->stepOverButton->Size = System::Drawing::Size(76, 22);
			this->stepOverButton->Text = L"Step over";
			this->stepOverButton->ToolTipText = L"Step over";
			this->stepOverButton->Click += gcnew System::EventHandler(this, &MyForm::stepOverButton_Click);
			// 
			// stepIntoButton
			// 
			this->stepIntoButton->Image = (cli::safe_cast<System::Drawing::Image ^>(resources->GetObject(L"stepIntoButton.Image")));
			this->stepIntoButton->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->stepIntoButton->Name = L"stepIntoButton";
			this->stepIntoButton->Size = System::Drawing::Size(74, 22);
			this->stepIntoButton->Text = L"Step into";
			this->stepIntoButton->Click += gcnew System::EventHandler(this, &MyForm::stepIntoButton_Click);
			// 
			// stepOutButton
			// 
			this->stepOutButton->Image = (cli::safe_cast<System::Drawing::Image ^>(resources->GetObject(L"stepOutButton.Image")));
			this->stepOutButton->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->stepOutButton->Name = L"stepOutButton";
			this->stepOutButton->Size = System::Drawing::Size(71, 22);
			this->stepOutButton->Text = L"Step out";
			this->stepOutButton->Click += gcnew System::EventHandler(this, &MyForm::stepOutButton_Click);
			// 
			// callstackColumnSymbol
			// 
			this->callstackColumnSymbol->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
			this->callstackColumnSymbol->FillWeight = 35;
			this->callstackColumnSymbol->HeaderText = L"Symbol";
			this->callstackColumnSymbol->Name = L"callstackColumnSymbol";
			this->callstackColumnSymbol->ReadOnly = true;
			this->callstackColumnSymbol->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// callstackColumnFile
			// 
			this->callstackColumnFile->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
			this->callstackColumnFile->FillWeight = 45;
			this->callstackColumnFile->HeaderText = L"File";
			this->callstackColumnFile->Name = L"callstackColumnFile";
			this->callstackColumnFile->ReadOnly = true;
			this->callstackColumnFile->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// callstackColumnLine
			// 
			this->callstackColumnLine->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
			this->callstackColumnLine->FillWeight = 20;
			this->callstackColumnLine->HeaderText = L"Line";
			this->callstackColumnLine->Name = L"callstackColumnLine";
			this->callstackColumnLine->ReadOnly = true;
			this->callstackColumnLine->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			this->callstackColumnLine->Width = 33;
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1157, 732);
			this->Controls->Add(this->toolStrip1);
			this->Controls->Add(this->splitContainer1);
			this->KeyPreview = true;
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			this->Shown += gcnew System::EventHandler(this, &MyForm::OnFormShown);
			this->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::OnKeyDown);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize ^>(this->codeTable))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize ^>(this->callstackTable))->EndInit();
			this->splitContainer1->Panel1->ResumeLayout(false);
			this->splitContainer1->Panel2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize ^>(this->splitContainer1))->EndInit();
			this->splitContainer1->ResumeLayout(false);
			this->splitContainer3->Panel1->ResumeLayout(false);
			this->splitContainer3->Panel2->ResumeLayout(false);
			this->splitContainer3->Panel2->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize ^>(this->splitContainer3))->EndInit();
			this->splitContainer3->ResumeLayout(false);
			this->splitContainer2->Panel1->ResumeLayout(false);
			this->splitContainer2->Panel2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize ^>(this->splitContainer2))->EndInit();
			this->splitContainer2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize ^>(this->symbolsTable))->EndInit();
			this->toolStrip1->ResumeLayout(false);
			this->toolStrip1->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion

	};
}
