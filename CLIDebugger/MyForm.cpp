#include "MyForm.h"

#include <string>
#include <msclr/marshal.h>

#include "comutil.h"
#include "vcclr.h"

#include "../Debugger/Debugger.h"
#include "../Debugger/StringUtils.h"
#include "../Debugger/Symbols.h"

#pragma managed

using namespace System;
using namespace System::Windows::Forms;
using namespace System::Runtime::InteropServices;
using namespace CPPCLITest;

Debugger sDebugger;

void CLIStringToCString(String ^cliStr, char *cStr)
{
	pin_ptr<const wchar_t> wch = PtrToStringChars(cliStr);
	size_t strSize = wcslen(wch) + 1;
	size_t convertedChars = 0;
	wcstombs_s(&convertedChars, cStr, strSize, wch, _TRUNCATE);
}

void OnOutputString(const char *message)
{
	String ^messageStr = msclr::interop::marshal_as<String^>(message);
	messageStr = messageStr->Replace("\n", "\r\n");
	MyForm::formInstance->Invoke(gcnew Action<String ^>(MyForm::formInstance, &MyForm::AppendTextToOutputBox), messageStr);
}

void BuildSymbolRow(Symbol &symbol, int indentLevel)
{
	String ^symbolNameStr = msclr::interop::marshal_as<String ^>(symbol.name);
	for (int indentIdx = 0; indentIdx < indentLevel; ++indentIdx)
		symbolNameStr = symbolNameStr->Insert(0, "|   ");
	if (symbol.basicType == BasicType::Pointer)
		symbolNameStr = symbolNameStr->Insert(0, "*");

	// Format value
	char valueBuffer[64];
	switch (symbol.basicType)
	{
	case BasicType::Char:
		sprintf_s(valueBuffer, "%c", symbol.data.asChar);
		break;
	case BasicType::WChar:
		sprintf_s(valueBuffer, "%c", symbol.data.asWChar);
		break;
	case BasicType::Int:
		sprintf_s(valueBuffer, "%d", symbol.data.asInt);
		break;
	case BasicType::UInt:
		sprintf_s(valueBuffer, "%d", symbol.data.asUInt);
		break;
	case BasicType::Long:
		sprintf_s(valueBuffer, "%d", symbol.data.asLong);
		break;
	case BasicType::ULong:
		sprintf_s(valueBuffer, "%d", symbol.data.asULong);
		break;
	case BasicType::Float:
		sprintf_s(valueBuffer, "%f", symbol.data.asFloat);
		break;
	case BasicType::UDT:
		sprintf_s(valueBuffer, "%s", symbol.data.asUDTInfo->name);
		break;
	case BasicType::Pointer:
		sprintf_s(valueBuffer, "0x%p", (void *)symbol.data.asPointerInfo->address);
		symbol = *symbol.data.asPointerInfo->symbol;
		break;
	default:
		sprintf_s(valueBuffer, "??? (%zx)", symbol.data.bytes);
		break;
	}
	String ^symbolValueStr = msclr::interop::marshal_as<String ^>(valueBuffer);

	// Build row array
	array<String ^> ^row = gcnew array<String ^>(2);
	row[0] = symbolNameStr;
	row[1] = symbolValueStr;

	// Need to make a parameter array
	array<array<Object ^> ^> ^params = gcnew array<array<Object ^> ^>(1);
	params[0] = row;

	// Call add row function
	MyForm::formInstance->Invoke(gcnew Action<array<Object ^> ^>(MyForm::formInstance, &MyForm::AddLocalSymbol), params);

	if (symbol.basicType == BasicType::UDT)
		for (unsigned int childIdx = 0; childIdx < symbol.data.asUDTInfo->childrenCount; ++childIdx)
		{
			BuildSymbolRow(*(symbol.data.asUDTInfo->children + childIdx), indentLevel + 1);
		}
}

void OnNewCallstack(CallstackEntry *callstackEntries, DWORD callstackEntryCount)
{
	// Clear callstack table
	MyForm::formInstance->Invoke(gcnew Action(MyForm::formInstance, &MyForm::ClearCallstack));

	for (DWORD frameIdx = 0; frameIdx < callstackEntryCount; ++frameIdx)
	{
		CallstackEntry &currentCsEntry = callstackEntries[frameIdx];
		String ^symbolNameStr = msclr::interop::marshal_as<String^>(currentCsEntry.name);
		String ^fileNameStr = msclr::interop::marshal_as<String^>(currentCsEntry.lineFilename);

		// Build row array
		array<String ^> ^row = gcnew array<String ^>(3);
		row[0] = symbolNameStr;
		row[1] = fileNameStr;
		char buffer[Str::DEFAULT_SIZE];
		Str::FromInt(currentCsEntry.lineNumber, buffer);
		String ^lineNumberStr = msclr::interop::marshal_as<String ^>(buffer);
		row[2] = lineNumberStr;

		// Need to make a parameter array
		array<array<Object ^> ^> ^params = gcnew array<array<Object ^> ^>(1);
		params[0] = row;

		// Call add row function
		MyForm::formInstance->Invoke(gcnew Action<array<Object ^> ^>(MyForm::formInstance, &MyForm::AddCallstackEntry), params);
	}

	// Request locals
	MyForm::formInstance->Invoke(gcnew Action(MyForm::formInstance, &MyForm::ClearLocalSymbols));

	Symbol symbols[64] = {};
	size_t symbolCount = sDebugger.GetLocalSymbols(0, symbols, 64);
	for (DWORD symbolIdx = 0; symbolIdx < symbolCount; ++symbolIdx)
	{
		Symbol &symbol = symbols[symbolIdx];
		BuildSymbolRow(symbol, 0);
	}
}

void MyForm::ClearCallstack()
{
	callstackTable->Rows->Clear();
}

void MyForm::ClearLocalSymbols()
{
	symbolsTable->Rows->Clear();
}

void MyForm::AddCallstackEntry(array<Object ^> ^row)
{
	callstackTable->Rows->Add(row);
}

void MyForm::AddLocalSymbol(array<Object ^> ^row)
{
	symbolsTable->Rows->Add(row);
}

void MyForm::AppendTextToOutputBox(String ^message)
{
	outputBox->Text = String::Concat(outputBox->Text, message);
}

[STAThread]
void Main(array<String^>^ args)
{
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);

	sDebugger.SetOnNewCallstack(OnNewCallstack);
	sDebugger.SetOnOutputString(OnOutputString);

	//MyForm form;
	MyForm::formInstance = gcnew MyForm(args[0]);

	char targetExecutable[MAX_PATH];
	CLIStringToCString(args[0], targetExecutable);
	sDebugger.Start(targetExecutable);

	Application::Run(MyForm::formInstance);
}

Void MyForm::OnFormShown(Object ^sender, EventArgs ^e)
{
	sDebugger.SetReady(true);
}

Void MyForm::OnKeyDown(Object ^sender, KeyEventArgs ^e)
{
	if (e->KeyCode == Keys::F5)
	{
		sDebugger.Continue();
	}

	else if (e->KeyCode == Keys::F9)
	{
		if (codeTable->SelectedRows->Count > 0)
		{
			const int line = codeTable->SelectedRows[0]->Index + 1;
			char file[MAX_PATH];
			CLIStringToCString(mCurrentFile, file);
			bool breakpointSet = sDebugger.ToggleBreakpoint(file, line);

			char lineStr[Str::DEFAULT_SIZE];
			Str::FromInt(line, lineStr);
			codeTable->SelectedRows[0]->Cells[codeColumnBreakpoints->Index]->Value = breakpointSet;
		}
	}

	else if (e->KeyCode == Keys::F10)
	{
		sDebugger.StepOver();
		e->Handled = true;
	}

	else if (e->KeyCode == Keys::F11)
	{
		e->Shift ? sDebugger.StepOut() : sDebugger.StepInto();
		e->Handled = true;
	}

	else if (e->KeyCode == Keys::O && e->Control)
	{
		OpenFileDialog ^fileDialog = gcnew OpenFileDialog();
		fileDialog->ShowDialog();
		OpenFile(fileDialog->FileName);
	}
}

Void MyForm::continueButton_Click(Object ^sender, EventArgs ^e)
{
	sDebugger.Continue();
}

Void MyForm::pauseButton_Click(Object ^sender, EventArgs ^e)
{
	sDebugger.Pause();
}

Void MyForm::stepOverButton_Click(Object ^sender, EventArgs ^e)
{
	sDebugger.StepOver();
}

Void MyForm::stepIntoButton_Click(Object ^sender, EventArgs ^e)
{
	sDebugger.StepInto();
}

Void MyForm::stepOutButton_Click(Object ^sender, EventArgs ^e)
{
	sDebugger.StepOut();
}

Void MyForm::codeTable_MouseDown(Object ^sender, MouseEventArgs ^e)
{
	DataGridView::HitTestInfo ^hitInfo;
	hitInfo = codeTable->HitTest(e->X, e->Y);

	if (hitInfo->Type == DataGridViewHitTestType::Cell)
	{
		if (hitInfo->ColumnIndex == codeColumnBreakpoints->Index)
		{
			char currentFile[MAX_PATH];
			CLIStringToCString(mCurrentFile, currentFile);
			bool breakpointPlaced = sDebugger.ToggleBreakpoint(currentFile, hitInfo->RowIndex + 1);
			codeTable->Rows[hitInfo->RowIndex]->Cells[codeColumnBreakpoints->Index]->Value = breakpointPlaced;
		}
	}
}

bool MyForm::OpenFile(String ^filename)
{
	if (filename->ToLowerInvariant()->Equals(mCurrentFile))
		// Already loaded
		return true;

	char cFilename[MAX_PATH];
	CLIStringToCString(filename, cFilename);

	HANDLE fileHandle = CreateFileA(cFilename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, NULL, NULL);
	if (fileHandle == INVALID_HANDLE_VALUE)
		return false;

	LARGE_INTEGER fileSize;
	if (!GetFileSizeEx(fileHandle, &fileSize))
		return false;

	LPVOID buffer = VirtualAlloc(0, (size_t)fileSize.QuadPart, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	if (buffer)
	{
		DWORD bytesRead;
		if (ReadFile(fileHandle, buffer, fileSize.LowPart, &bytesRead, 0))
		{
			String ^fileContents = msclr::interop::marshal_as<String ^>((char *)buffer);
			fileContents = fileContents->Replace(gcnew String("\t"), gcnew String("    "));
			array<String ^> ^fileLines = fileContents->Split('\n');

			codeTable->Rows->Clear();

			for (int idx = 0; idx < fileLines->Length; ++idx)
			{
				char buffer[Str::DEFAULT_SIZE];
				Str::FromInt(idx + 1, buffer);
				String ^lineNumberStr = msclr::interop::marshal_as<String ^>(buffer);
				array<Object ^> ^row = gcnew array<Object ^>(3);
				row[0] = false;
				row[1] = lineNumberStr;
				row[2] = fileLines[idx];
				codeTable->Rows->Add(row);
			}
			mCurrentFile = filename->ToLowerInvariant();
		}
		VirtualFree(buffer, 0, MEM_RELEASE);
	}
	CloseHandle(fileHandle);
	return true;
}

Void MyForm::callstackTable_SelectionChanged(Object ^sender, EventArgs ^e)
{
	DataGridView ^grid = (DataGridView ^)sender;
	if (grid->SelectedRows->Count > 0)
	{
		String ^filename = (String ^)grid->SelectedRows[0]->Cells[1]->Value;
		if (!String::IsNullOrEmpty(filename))
		{
			if (OpenFile(filename))
			{
				// Select line
				String ^lineStr = (String ^)grid->SelectedRows[0]->Cells[2]->Value;
				char cLineStr[8];
				CLIStringToCString(lineStr, cLineStr);
				int line = atoi(cLineStr);

				codeTable->ClearSelection();
				codeTable->Rows[line - 1]->Selected = true;

				const int currentFirstRow = codeTable->FirstDisplayedScrollingRowIndex;
				const int visibleRows = codeTable->DisplayedRowCount(true);
				const bool rowVisible = line - 1 >= currentFirstRow && line - 1 < currentFirstRow + visibleRows;
				if (!rowVisible)
					// Scroll to line
					codeTable->FirstDisplayedScrollingRowIndex = line - 1 - visibleRows / 2;
			}
		}
		// Load locals on new selected stack frame
		MyForm::formInstance->Invoke(gcnew Action(MyForm::formInstance, &MyForm::ClearLocalSymbols));

		Symbol symbols[64];
		int stackFrame = grid->SelectedRows[0]->Index;
		size_t symbolCount = sDebugger.GetLocalSymbols(stackFrame, symbols, 64);
		for (DWORD symbolIdx = 0; symbolIdx < symbolCount; ++symbolIdx)
		{
			Symbol &symbol = symbols[symbolIdx];
			BuildSymbolRow(symbol, 0);
		}
	}
}
