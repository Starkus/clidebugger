#include <stdio.h>
#include <thread>

struct Pumpkin
{
	float weight;
	int pumpkinness;
};

int addOne(int input)
{
	input++;
	return input;
}

int twoFunctions(int input)
{
	return input + 2;
}

int inOneLine(int input)
{
	return input * input;
}

void anotherThread()
{
	int threadVar = 8;
	while (true)
	{
		threadVar = threadVar ? 0 : 8;
		printf("Secondary thread working\n");
	}
}

void modifyPumpkin(Pumpkin *pumpkin)
{
	++pumpkin->pumpkinness;
}

int main(int argc, char **argv)
{
	int variable = 0;

	Pumpkin pumpkin = { 0.5f, 24 };

	std::thread secondaryThread(anotherThread);

	while (true)
	{
		int result = addOne(variable);
		int anotherResult = twoFunctions(variable) + inOneLine(variable);

		if (anotherResult > result)
			variable = result;
		else
			variable = result;

		modifyPumpkin(&pumpkin);

		printf("1 usd is worth %d bvr\n", variable);
	}

	secondaryThread.join();

	return 0;
}
